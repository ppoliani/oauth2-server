
//region Import

var mongoose = require('mongoose');

//endregion

//region Module

var AuthorizationCode = new mongoose.Schema({
    code: {
        type: String,
        required: true
    },

    clientId: {
        type: String,
        required: true
    },
    userId: {
        type: String,
        required: true
    },

    redirectURI: {
        type: String,
        required: true
    }
});

//endregion

//region Export

module.exports = AuthorizationCode;

//endregion