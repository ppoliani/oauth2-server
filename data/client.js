
//region Import

var mongoose = require('mongoose');

//endregion

//region Module

var Client = new mongoose.Schema({
    name: {
        type: String,
        unique: true,
        required: true
    },
    clientId: {
        type: String,
        unique: true,
        required: true
    },
    clientSecret: {
        type: String,
        required: true
    },
    redirectURI: {
        type: String,
        required: true
    }
});

//endregion

//region Export

module.exports = Client;

//endregion