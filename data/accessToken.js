
//region Import

var mongoose = require('mongoose');

//endregion

//region Module

var AccessToken = new mongoose.Schema({
    userId: {
        type: String,
        required: false
    },
    clientId: {
        type: String,
        required: true
    },
    token: {
        type: String,
        unique: true,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

//endregion

//region export

module.exports = AccessToken;

//endregion