
//region Import

var mongoose            = require('mongoose'),
    log                 = require('../config/logger')(module),
    config              = require('../config/index'),
    User                = require('./user'),
    Client              = require('./client'),
    AccessToken         = require('./accessToken'),
    AuthorizationCode   = require('./authorizationCode'),
    RefreshToken        = require('./refereshToken'),
    Claim               = require('./claim').ClaimModel;

//endregion

// region Private Fields

var mongodb_connection_string = config.get('mongoose:uri');

if(process.env.OPENSHIFT_MONGODB_DB_URL){
    // ToDo: this will be used when we use mongodb on openshift
    // mongodb_connection_string = process.env.OPENSHIFT_MONGODB_DB_URL + 'oauth';
}

if(process.env.OPENSHIFT_NODEJS_PORT){
    mongodb_connection_string = 'mongodb://admin:MYFrEimLrTQ1@kahana.mongohq.com:10061/oauth';
}

var conn = mongoose.createConnection(mongodb_connection_string);

// endregion

//region Inner Methods

var
    /**
     * Initializes the db
     */
    initDb = function initDb(){
        conn.on('error', function (err) {
            log.error('connection error:', err.message);
        });
        conn.once('open', function callback () {
            log.info("Connected to DB!");
        });

        _registerModels();
    },

    /**
     * Returns the given mongoose model
     * @param model
     */
    getModel = function(model){
        return conn.model(model);
    },

    /**
     * Registers mongoose models
     * @private
     */
    _registerModels = function(){
        // register models
        conn.model('User', User),
        conn.model('Client', Client),
        conn.model('AccessToken', AccessToken),
        conn.model('AuthorizationCode', AuthorizationCode),
        conn.model('RefreshToken', RefreshToken);
        conn.model('Claim', Claim);

        // Create a client for our main app
        // ToDo: this will ultimately become more configurable
        var ClientModel = conn.model('Client');
        var createClient = function(clientData){
            ClientModel.findOne(clientData, function(err, clientModel){
                if(!clientModel){
                    var client = new ClientModel(clientData);
                    client.save(function(err, client) {
                        if(err) return log.error(err);
                        else log.info("New client - %s:%s",client.clientId,client.clientSecret);
                    });
                }
            });
        };

        createClient({ name: "Titlr App", clientId: "titlrApp", clientSecret:"titlrApp2014", redirectURI: 'http://localhost:9009/auth/example-oauth2orize/callback' });
        createClient({ name: "Samplr", clientId: "abc123", clientSecret:"sshsecret", redirectURI: 'http://localhost:9009/auth/example-oauth2orize/callback' });
    };

// endregion

//region Export

module.exports = {
    initDb: initDb,
    getModel: getModel
};

//endregion