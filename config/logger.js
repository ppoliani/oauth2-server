
//region Import

var winston = require('winston');

//endregion

//region Module

function getLogger(module) {
    var path = module.filename.split('/').slice(-2).join('/');

    return new winston.Logger({
        transports : [
            new winston.transports.Console({
                colorize:   true,
                level:      'debug',
                label:      path
            })
        ]
    });
}

//endregion

//region Export

module.exports = getLogger;

//endregion