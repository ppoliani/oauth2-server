
//region Import

var oauth2orize             = require('oauth2orize'),
    passport                = require('passport'),
    crypto                  = require('crypto'),
    jwt                     = require('jwt-simple'),
    config                  = require('../config/'),
    login                   = require('connect-ensure-login'),
    UserModel               = require('../data').getModel('User'),
    ClientModel             = require('../data').getModel('Client'),
    AccessTokenModel        = require('../data').getModel('AccessToken'),
    RefreshTokenModel       = require('../data').getModel('RefreshToken'),
    AuthorizationCodeModel  = require('../data').getModel('AuthorizationCode');

//endregion

//region Module

// create OAuth 2.0 server
var server = oauth2orize.createServer();

/**
 * Register serialialization and deserialization functions.
 * When a client redirects a user to user authorization endpoint, an
 * authorization transaction is initiated.  To complete the transaction, the
 * user must authenticate and approve the authorization request.  Because this
 * may involve multiple HTTP request/response exchanges, the transaction is
 * stored in the session.

 * An application must supply serialization functions, which determine how the
 * client object is serialized into the session.  Typically this will be a
 * simple matter of serializing the client's ID, and deserializing by finding
 * the client by ID from the database.
 */
server.serializeClient(function(client, done) {
    return done(null, client.id);
});

server.deserializeClient(function(id, done) {
    ClientModel.findById(id, function(err, client) {
        if (err) { return done(err); }
        return done(null, client);
    });
});

/**
 * Register supported grant types.
 * OAuth 2.0 specifies a framework that allows users to grant client
 * applications limited access to their protected resources.  It does this
 * through a process of the user granting access, and the client exchanging
 * the grant for an access token.

 * Grant authorization codes.  The callback takes the `client` requesting
 * authorization, the `redirectURI` (which is used as a verifier in the
 * subsequent exchange), the authenticated `user` granting access, and
 * their response, which contains approved scope, duration, etc. as parsed by
 * the application.  The application issues a code, which is bound to these
 * values, and will be exchanged for an access token.
 */
server.grant(oauth2orize.grant.code(function(client, redirectURI, user, ares, done) {
    var code = crypto.randomBytes(32).toString('base64'),
        authorizationCode = new AuthorizationCodeModel({
            code: code,
            clientId: client.id,
            userId: user.id,
            redirectURI: redirectURI
        });

    authorizationCode.save(function(err) {
        if (err) { return done(err); }
        done(null, code);
    });
}));

/**
 * Grant implicit authorization.  The callback takes the `client` requesting
 * authorization, the authenticated `user` granting access, and
 * their response, which contains approved scope, duration, etc. as parsed by
 * the application.  The application issues a token, which is bound to these
 * values.
 */
server.grant(oauth2orize.grant.token(function(client, user, ares, done) {
    saveToken(client, user, done);
}));

/**
 * Exchange authorization codes for access tokens.  The callback accepts the
 * `client`, which is exchanging `code` and any `redirectURI` from the
 * authorization request for verification.  If these values are validated, the
 * application issues an access token on behalf of the user who authorized the
 * code.
 */
server.exchange(oauth2orize.exchange.code(function(client, code, redirectURI, done) {
    AuthorizationCodeModel.findOne({ clientId: client.id, redirectURI: redirectURI, code: code }, function(err, authCode) {
        if (err) { return done(err); }
        if (client.id !== authCode.clientId) { return done(null, false); }
        if (redirectURI !== authCode.redirectURI) { return done(null, false); }

        saveToken(client, { id: authCode.userId }, done);
    });
}));

/**
 * Exchange the client id and password/secret for an access token.  The callback accepts the
 * `client`, which is exchanging the client's id and password/secret from the
 * authorization request for verification. If these values are validated, the
 * application issues an access token on behalf of the client who authorized the code.
 */
server.exchange(oauth2orize.exchange.clientCredentials(function(client, scope, done) {
    //Validate the client
    ClientModel.findOne({ clientId: client.clientId }, function(err, localClient) {
        if (err) { return done(err); }
        if(localClient === null) {
            return done(null, false);
        }
        if(localClient.clientSecret !== client.clientSecret) {
            return done(null, false);
        }

        //Pass in a null for user id since there is no user with this grant type
        saveToken(client, null, done);
    });
}));

/**
 * Exchange refreshToken for access token.
 */
server.exchange(oauth2orize.exchange.refreshToken(function(client, refreshToken, scope, done) {
    RefreshTokenModel.findOne({ token: refreshToken }, function(err, token) {
        if (err) { return done(err); }
        if (!token) { return done(null, false); }
        if (!token) { return done(null, false); }

        UserModel.findById(token.userId, function(err, user) {
            if (err) { return done(err); }
            if (!user) { return done(null, false); }

            RefreshTokenModel.remove({ userId: user.userId, clientId: client.clientId }, function (err) {
                if (err) return done(err);
            });
            AccessTokenModel.remove({ userId: user.userId, clientId: client.clientId }, function (err) {
                if (err) return done(err);
            });

            var tokenValue = crypto.randomBytes(32).toString('base64');
            var refreshTokenValue = crypto.randomBytes(32).toString('base64');
            var token = new AccessTokenModel({ token: tokenValue, clientId: client.clientId, userId: user.userId });
            var refreshToken = new RefreshTokenModel({ token: refreshTokenValue, clientId: client.clientId, userId: user.userId });
            refreshToken.save(function (err) {
                if (err) { return done(err); }
            });
            var info = { scope: '*' }
            token.save(function (err, token) {
                if (err) { return done(err); }
                done(null, tokenValue, refreshTokenValue, { 'expires_in': config.get('security:tokenLife') });
            });
        });
    });
}));

/**
 * user authorization endpoint
 * `authorization` middleware accepts a `validate` callback which is
 * responsible for validating the client making the authorization request.  In
 * doing so, is recommended that the `redirectURI` be checked against a
 * registered value, although security requirements may vary accross
 * implementations.  Once validated, the `done` callback must be invoked with
 * a `client` instance, as well as the `redirectURI` to which the user will be
 * redirected after an authorization decision is obtained.

 * This middleware simply initializes a new authorization transaction.  It is
 * the application's responsibility to authenticate the user and render a dialog
 * to obtain their approval (displaying details about the client requesting
 * authorization).  We accomplish that here by routing through `ensureLoggedIn()`
 * first, and rendering the `dialog` view.
 */
exports.authorization = [
    login.ensureLoggedIn(),
    server.authorization(function(clientID, redirectURI, done) {
        ClientModel.findOne({ clientId: clientID }, function(err, client) {
            if (err) { return done(err); }
            if(client.redirectURI !== redirectURI) { return done('Wrong redirect URI'); }
            return done(null, client, redirectURI);
        });
    }),
    function(req, res){
        res.render('dialog', { transactionID: req.oauth2.transactionID, user: req.user, client: req.oauth2.client });
    }
];

/**
 * Exchange username & password for access token.
 * i.e. Resource Owner Password credentials
 */
server.exchange(oauth2orize.exchange.password(function(client, username, password, scope, done) {
    UserModel.findOne({ username: username }, function(err, user) {
        if (err) { return done(err); }
        if (!user) { return done(null, false); }
        if (!user.validPassword(password)) { return done(null, false); }

        RefreshTokenModel.remove({ userId: user.id, clientId: client.clientId }, function (err) {
            if (err) return done(err);
        });
        AccessTokenModel.remove({ userId: user.id, clientId: client.clientId }, function (err) {
            if (err) return done(err);
        });

        var tokenValue =  {
                expires_in: config.get('security:tokenLife'),
                userName: user.username,
                claims: user.claims
            },

            jwtToken = jwt.encode(tokenValue, config.get('security:jwt_secret')),
            refreshTokenValue = crypto.randomBytes(32).toString('base64'),

            token = new AccessTokenModel({
                token: jwtToken,
                clientId: client.clientId,
                userId: user.id,
                created: new Date()
            }),

            refreshToken = new RefreshTokenModel({
                token: refreshTokenValue,
                clientId: client.clientId,
                userId: user.id
            });

        refreshToken.save(function (err) {
            if (err) { return done(err); }
        });

        token.save(function (err, token) {
            if (err) { return done(err); }

            // jwtToken contains user_info but it's encoded, thus we explicitly return the sure info with this request
            done(null, jwtToken, refreshTokenValue, { user_info: { userName: user.username, id: user.id, claims: user.claims }});
        });
    });
}));

/**
 * user decision endpoint
 *`decision` middleware processes a user's decision to allow or deny access
 * requested by a client application.  Based on the grant type requested by the
 * client, the above grant middleware configured above will be invoked to send
 * a response.
 */
exports.decision = [
    login.ensureLoggedIn(),
    server.decision()
];

/**
 * Saves a token with the given information
 * @param client
 * @param user
 * @param done
 */
var saveToken = function(client, user, done){
    var token = crypto.randomBytes(32).toString('base64'),
        accessToken = new AccessTokenModel({
            userId: user.id,
            clientId: client.id,
            token: token
        });

    accessToken.save(function(err) {
        if (err) { return done(err); }
        done(null, token);
    });
};

//endregion

//region Export

/**
 * token endpoint
 * `token` middleware handles client requests to exchange authorization grants
 * for access tokens.  Based on the grant type being exchanged, the above
 * exchange middleware will be invoked to handle the request.  Clients must
 * authenticate when making requests to this endpoint.
 */
exports.token = [
    passport.authenticate(['basic', 'oauth2-client-password'], { session: false }),
    server.token(),
    server.errorHandler()
];

//endregion