
//region Import

var config                  = require('../config/');
    passport                = require('passport'),
    BasicStrategy           = require('passport-http').BasicStrategy,
    ClientPasswordStrategy  = require('passport-oauth2-client-password').Strategy,
    BearerStrategy          = require('passport-http-bearer').Strategy,
    UserModel               = require('../data').getModel('User'),
    ClientModel             = require('../data').getModel('Client'),
    AccessTokenModel        = require('../data').getModel('AccessToken'),
    RefreshTokenModel       = require('../data').getModel('RefreshToken'),
    LocalStartegy           = require('passport-local').Strategy,
    FacebookStrategy        = require('passport-facebook').Strategy,
    configAuth              = require('./config'),
    emailManager            = require('../email/email-manager');


//endregion

//region Module

/**
 * BasicStrategy & ClientPasswordStrategy
 *
 * These strategies are used to authenticate registered OAuth clients.  They are
 * employed to protect the `token` endpoint, which consumers use to obtain
 * access tokens.  The OAuth 2.0 specification suggests that clients use the
 * HTTP Basic scheme to authenticate.  Use of the client password strategy
 * allows clients to send the same credentials in the request body (as opposed
 * to the `Authorization` header).  While this approach is not recommended by
 * the specification, in practice it is quite common.
 */
passport.use(new BasicStrategy(
    function(username, password, done) {
        ClientModel.findOne({clientId: username}, function(err, client) {
            if (err) { return done(err); }
            if (!client) { return done(null, false); }
            if (client.clientSecret != password) { return done(null, false); }
            return done(null, client);
        });
    }
));


passport.use(new ClientPasswordStrategy(
    function(clientId, clientSecret, done) {
        ClientModel.findOne({ clientId: clientId }, function(err, client) {
            if (err) { return done(err); }
            if (!client) { return done(null, false); }
            if (client.clientSecret != clientSecret) { return done(null, false); }

            return done(null, client);
        });
    }
));

passport.use(new BearerStrategy(
    function(accessToken, done) {
        AccessTokenModel.findOne({ token: accessToken }, function(err, token) {
            if (err) { return done(err); }
            if (!token) { return done(null, false); }

            if( Math.round((Date.now()-token.created)/1000) > config.get('security:tokenLife') ) {
                AccessTokenModel.remove({ token: accessToken }, function (err) {
                    if (err) return done(err);
                });

                // ToDo: instead redirect to login page
                return done(null, false, { message: 'Token expired' });
            }

            UserModel.findById(token.userId, function(err, user) {
                if (err) { return done(err); }
                if (!user) { return done(null, false, { message: 'Unknown user' }); }

                var info = { scope: '*', claims: user.claims }
                done(null, user, info);
            });
        });
    }
));

/**
 * required for persistent login sessions
 * passport needs ability to serialize and deserialize users out of session
 */

/**
 * used to serialize the user for the session
 */
passport.serializeUser(function(user, done){
    done(null, user.id);
});

/**
 * used to deserialize the user
 */
passport.deserializeUser(function(id, done){
    UserModel.findById(id, function(err, user){
        done(err, user);
    });
});

/**
 * Local SingUp
 * we are using named strategies since we have one for login and one for signup
 * by default, if there was no name, it would just be called 'local'
 */
passport.use('local-signup', new LocalStartegy({
        // by default local strategy uses username and password, we will override with email
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass the entire request back to the callback
    },
    function(req, username, password, done){
        // async
        // User.findOne won't fire unless data is sent back
        process.nextTick(function(){
            var email = req.body.email,
                username = req.body.username;

            // find a user whose email is the same as the forms email
            // we are checking to see if the user trying to login already exists
            UserModel.findOne({ $or: [ { 'username': username }, { 'local.email': email } ] }, function(err, user){
                if(err){
                    return done(err);
                }

                if(user && user.username === username){
                    return done({ message: 'That user name is already taken' }, false);
                }
                else if(user && user.local.email === email){
                    return done({ message: 'That email is already taken' }, false);
                }
                else {
                    var newUser = new UserModel();

                    // Set the user's local credentials
                    newUser.local.email = email;
                    newUser.username = username;
                    newUser.local.password = password;

                    newUser.save(function(err){
                        if(err){
                            throw err;
                        }

                        emailManager.sendWelcomeEmail({
                            email: email,
                            username: username
                        });

                        return done(null, newUser);
                    });
                }

            });
        });
    }));

/**
 * Local Login
 * we are using named strategies since we have one for login and one for signup
 * by default, if there was no name, it would just be called 'local'
 */
passport.use('local-login', new LocalStartegy({
    usernameField : 'email',
    passwordField : 'password',
    passReqToCallback : true
}, function(req, email, password, done) { // callback with email and password from our form
    // find a user whose email is the same as the forms email
    // we are checking to see if the user trying to login already exists
    UserModel.findOne({ 'local.email' :  email }, function(err, user) {
        // if there are any errors, return the error before anything else
        if (err)
            return done(err);

        // if no user is found, return the message
        if (!user)
            return done('No user found', false);

        // if the user is found but the password is wrong
        if (!user.validPassword(password))
            return done('Wrong password', false);

        // all is well, return successful user
        return done(null, user);
    });
}));

//endregion