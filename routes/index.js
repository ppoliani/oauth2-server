
//region Import

var passport     = require('passport'),
    oauth2       = require('../security/oauth2'),
    security     = require('../security'),
    apiEndpoint  = require('oauth-api-endpoint');

//endregion

//region Inner Methods

function setUpRoutes(app){

    var api = apiEndpoint(app, passport);

    /* GET home page. */
    app.get('/',  passport.authenticate('bearer', {
        session: true,
        //successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    app.get('/login', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('login.ejs', { message: req.flash('loginMessage') });
    });

    // process the login form
    app.post('/login', passport.authenticate('local-login', {
        session: true
    }));

    app.get('/signup', function(req, res) {
        // render the page and pass in any flash data if it exists
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    // process the signup form
    app.post('/signup', function(req, res){

        passport.authenticate('local-signup', { session: false })(req, res, function(err){
            if(err){
                res.send(200, err);
            } else {
                res.send(204);
            }
        });
    });

    app.get('/profile',  passport.authenticate('bearer', { session: false }),function(req, res) {
        res.render('profile.ejs', {
            user : req.user // get the user out of session and pass to template
        });
    });

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/dialog/authorize', oauth2.authorization);
    app.post('/dialog/authorize/decision', oauth2.decision);
    app.post('/oauth/token', oauth2.token);

    app.get('/api/v1/clientinfo', [
        passport.authenticate('bearer', { session: false }),
        function(req, res) {
            // req.authInfo is set using the `info` argument supplied by
            // `BearerStrategy`.  It is typically used to indicate scope of the token,
            // and used in access control checks.  For illustrative purposes, this
            // example simply returns the scope in the response.
            res.json({ client_id: req.user.id, name: req.user.name, scope: req.authInfo.scope })
        }
    ]);
}

//endregion

//region Export

module.exports = setUpRoutes;

//endregion